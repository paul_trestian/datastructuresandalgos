package com.trestian.sda.fundamentals.main.datastructures;

public class Set {

	private int[] values = new int[10];

	/**
	 * This method is used to add elements in list.
	 * 
	 * @param value
	 *            - value to add inside the array
	 * @param position
	 *            - position / index where the value is added at
	 */
	public void add(int value, int position) {
		if (!isDuplicate(value)) {
			checkSize(position);
			values[position] = value;
		}
	}

	public boolean isDuplicate(int value) {
		for (int i=0; i< values.length; i++) {
			if (values[i] == value) {
				return true;
			}
		}
		
		return false;
		
	}
	
	public int get(int position) {
		return values[position];
	}

	public int size() {
		return values.length;
	}

	public void checkSize(int position) {
		if (position > values.length - 1) {
			int[] valuesDoubleSize = new int[values.length * 2];
			for (int i = 0; i < values.length; i++) {
				valuesDoubleSize[i] = values[i];
			}
			values = valuesDoubleSize;
		}
	}

	public void print() {
		for (int i = 0; i < values.length - 1; i++) {
			// myList.add(i, i-1);
			System.out.print(values[i] + ", ");
		}
		System.out.println(values[values.length - 1]);
	}

}
