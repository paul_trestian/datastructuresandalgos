package com.trestian.sda.fundamentals.main.datastructures;

public class List {

	private Object[] values = new Object[10];
	
	/**
	 * This method is used to add elements in list. 
	 * 
	 * @param value - value to add inside the array
	 * @param position - position / index where the value is added at
	 */
	public void add(Object value, int position) {
		checkSize(position);
		values[position] = value;
	}
	
	public Object get(int position) {
		return values[position];
	}
	
	public int size() {
		return values.length;
	}
	
	public void checkSize(int position) {
		if (position > values.length-1) {
			Object[] valuesDoubleSize = new Object[values.length*2];
			for (int i=0; i<values.length; i++) {
				valuesDoubleSize[i] = values[i];
			}
			values = valuesDoubleSize;
		}
	}
	
	
	public void print() {
		for (int i=0; i<values.length-1; i++) {
//			myList.add(i, i-1);
			System.out.print(values[i] + ", ");
		}
		System.out.println(values[values.length-1]);
	}
}
