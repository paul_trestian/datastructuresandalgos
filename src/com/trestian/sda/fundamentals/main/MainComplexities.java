package com.trestian.sda.fundamentals.main;

import java.util.Random;
import java.util.Scanner;

public class MainComplexities {

	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print("Array size: ");
		int size = scan.nextInt();
		int[] numbers = new int[size];

		Random rand = new Random();
		
		for (int i=0; i<size; i++) {
			numbers[i]= rand.nextInt();
		} 
		
		for (int i=1; i<size; i=i*2) {
			System.out.println(numbers[i]);
		}
		
	}
}
