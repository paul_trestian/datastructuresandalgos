package com.trestian.sda.fundamentals.main;

import com.trestian.sda.fundamentals.main.datastructures.List;
import com.trestian.sda.fundamentals.main.datastructures.Set;

public class MainDataStructures {

	
	public static void main(String[] args) {
		List numbersList = new List();
		Set numbersSet = new Set();
		for (int i=0; i<numbersList.size(); i++) {
			numbersList.add(i, i);
			numbersSet.add(i, i);
		}
		numbersSet.add(11, 11);
		numbersSet.add(7, 12);
		numbersSet.add(7, 13);
		numbersSet.add(7, 14);
		numbersList.add(7, 11);
		numbersList.add(7, 12);
		numbersList.add(7, 13);
		numbersList.add(7, 14);
		
		numbersList.print();
		numbersSet.print();
//--------------------------------------------------------------------------------
		
		List namesList = new List();
		//List myList3 = new List();
		
		namesList.add("Paul", 0);
		
		 namesList.get(0);
		
		System.out.println(  );
		
		namesList.print();
		
	}
}
